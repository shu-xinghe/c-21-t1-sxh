#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define pi 3.14
int main()
{
	double r, h;
	scanf("%lf%lf", &r, &h);
	printf("圆周长为%.2lf\n", pi * 2 * r);
	printf("圆面积为%.2lf\n", pi*r*r);
	printf("圆球表面积为%.2lf\n", 4*pi*r*r);
	printf("圆球体积为%.2lf\n", 4 * pi*r*r*r / 3);
	printf("圆柱体积为%.2lf\n", pi*r*r*h);
	system("pause");
	return 0;}